<?php
return [
    'user.passwordResetTokenExpire' => 3600,
    'storageUri' => 'http://e-machine.com/uploads/',
    'storagePath' => '@frontend/web/uploads/',
];
