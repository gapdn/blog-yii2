<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

class Storage extends Component implements StorageInterface  {
    
    
    private $fileName;
    
    /**
     * Save given UploadedFile instance to disk
     * @param UploadedFile $file
     * @return string|null
     */
    public function saveUploadedFile(UploadedFile $file) {
        
        $path = $this->preparePath($file);
        
        if ($path && $file->saveAs($path)) {
            return $this->fileName;
        }
    }
    
    /**
     * Prepare path to save uploaded file
     * @param UploadedFile $file
     * @return string|null
     */
    protected function preparePath(UploadedFile $file) {
        
        $this->fileName = $this->getFileName($file);
        // f1/ds/f3355ae43452r2f3f.jpg
        
        $path  = $this->getStoragePath() . $this->fileName;
        // /var/www/emachine/frontend/web/uploads/f1/ds/f3355ae43452r2f3f.jpg
        
        $path = FileHelper::normalizePath($path);
        if (FileHelper::createDirectory(dirname($path))) {
            return $path;
        }
    }

    public function getFileName(UploadedFile $file) {
        
        // $file->tempName - tmp/qio93kf
        
        $hash = sha1($file->tempName); // f1dsf3355ae43452r2f3f
        
        $name = substr_replace($hash, '/', 2, 0);
        $name = substr_replace($name, '/', 5, 0);
        return $name . '.' . $file->extension; // f1/ds/f3355ae43452r2f3f
        
    }
    
    /**
     * @return string
     */
    protected function getStoragePath() {
        
        return Yii::getAlias(Yii::$app->params['storagePath']);
    }
    
    /**
     * 
     * @param string $fileName
     * @return string
     */
    public function getFile(string $fileName) {
        return Yii::$app->params['storageUri'] . $fileName;
    }

}
