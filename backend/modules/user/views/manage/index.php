<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'picture',
                'format' => 'raw',
                'value' => function ($user) {
                    /* @var $user backend\models\Usre */
                    return Html::img($user->getImage(), ['width' => '50px']);
                },
            ],
            'username',
            'email:email',
            'created_at:datetime',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($user) {
                    switch ($user->status) {
                        case 10:
                            return 'Active';
                        case 9:
                            return 'Inactive';
                        case 0:
                            return 'Deleted';
                    }
                }
            ],
            [
                'attribute' => 'roles',
                'value' => function ($user) {
                    return implode(',', $user->getRoles());
                }
            ],
             

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
