<?php

use yii\db\Migration;

/**
 * Class m190403_162029_alter_table_post_add_column_complaints
 */
class m190403_162029_alter_table_post_add_column_complaints extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%post}}', 'complaints', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%post}}', 'complaints');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190403_162029_alter_table_post_add_column_complaints cannot be reverted.\n";

        return false;
    }
    */
}
