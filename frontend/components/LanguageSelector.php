<?php

namespace frontend\components;

use yii\base\BootstrapInterface;

class LanguageSelector implements BootstrapInterface {
    
    public $supportedLanguages = ['en-US', 'ru-RU'];


    public function bootstrap($app) {
        
        $coockieLanguage = $app->request->cookies['language'];
        if (isset($coockieLanguage) && in_array($coockieLanguage, $this->supportedLanguages)) {
            $app->language = $coockieLanguage;
        }
    }

}
