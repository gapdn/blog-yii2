<?php
return [
    'adminEmail' => 'admin@example.com',
    'maxFileSize' => 1024 * 1024 * 2, // 2megabites
    
    'storageUri' => '/uploads/', // http://e-machine.com/uploads/f1/ds/f3355ae43.jpg
    
    'picture' => [
        'maxWidth' => 1024,
        'maxHeight' => 768,
    ],
    'feedPostLimit' => 200,
];
