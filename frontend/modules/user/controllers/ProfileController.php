<?php

namespace frontend\modules\user\controllers;

use yii\web\Controller;
use frontend\models\User;
use yii\web\NotFoundHttpException;
use Yii;
use frontend\modules\user\models\forms\PictureForm;
use \yii\web\UploadedFile;
use yii\web\Response;
use Intervention\Image\ImageManager;

/**
 * Default controller for the `user` module
 */
class ProfileController extends Controller
{
    public function actionView($nickname) {
        
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('warning', 'Pleas login');
            return $this->redirect('/user/default/login');
        }
        $currentUser = Yii::$app->user->identity;
        
        $modelPicture = new PictureForm();
        
        return $this->render('view', [
            'user' => $this->findUser($nickname),
            'currentUser' => $currentUser,
            'modelPicture' => $modelPicture,
        ]);
    }
    
    private function findUser($nickname) {
        
        if ($user = User::find()->where(['nickname' => $nickname])->orWhere(['id' => $nickname])->one()) {
            return $user;
        }
        throw new NotFoundHttpException;    
    }
    
    public function actionSubscribe($id) {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/default/login');
        }
        
        $currentUser = Yii::$app->user->identity;
        $user = $this->findUserById($id);
        
        $currentUser->followUser($user);
        return $this->redirect(['/user/profile/view', 'nickname' => $user->getNickname()]);
    }
    
    public function actionUnsubscribe($id) {
        
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/default/login');
        }
        
        $currentUser = Yii::$app->user->identity;
        $user = $this->findUserById($id);
        
        $currentUser->unfollowUser($user);
        
        return $this->redirect(['/user/profile/view', 'nickname' => $user->getNickname()]);
    }
    
    private function findUserById($id) {
        if ($user = User::findOne($id)) {
            return $user;
        }
        
        throw new NotFoundHttpException();
    }
    
    public function actionUploadPicture() {
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model = new PictureForm();
        $model->picture = UploadedFile::getInstance($model, 'picture');
        
        if ($model->validate()) {
            
            $width = Yii::$app->params['picture']['maxWidth'];
            $height = Yii::$app->params['picture']['maxHeight'];

            $manager = new ImageManager(array('driver' => 'imagick'));

            $image = $manager->make($model->picture->tempName); // /tmp/234uc

            $image->resize($width, $height, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save();
            
            $user = Yii::$app->user->identity;
            $user->picture = Yii::$app->storage->saveUploadedFile($model->picture); // 18/6f/1c7726b3aa80434fbd5209fc7b80e9dd2032.jpg
            
            if ($user->save(['picture'])) { // save without validation and only picture attribut
                
                return [
                    'success' => true,
                    'pictureUri' => Yii::$app->storage->getFile($user->picture),
                ];
                
            }
        }
        return ['success' => false, 'errors' => $model->getErrors()];
        
    }
    
    
//    public function actionGenerate() {
//        
//        $faker = \Faker\Factory::create();
//        
//        for ($i = 0; $i < 1000; $i++) {
//            $user = new User([
//                'username' => $faker->name,
//                'email' => $faker->email,
//                'about' => $faker->text(200),
//                'nickname' => $faker->regexify('[A-Za-z0-9_]{5,15}'),
//                'auth_key' => Yii::$app->security->generateRandomString(),
//                'password_hash' => Yii::$app->security->generateRandomString(),
//                'created_at' => $time = time(),
//                'updated_at' => $time,
//            ]);
//            $user->save(false);
//        }
//    }
}
